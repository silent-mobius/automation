# PAM: Plugable Authentication Module Lab

- Method used to auth users
- method can be used from central location


column 1: module interface types:
- auth: request and verifies passwords
- account: verifies that access is allowed
- password: allows for password change
- session: manages user session in his/her/their account

on column2: 
- required: flag that demands for exit status for the module to be sucesses full or exit code will be returned
  - requisit: same as required but it also notifies
- suffient: If a sufficient module succeeds, it is enough to satisfy the requirements of sufficient modules in that realm for use of the service, and modules below it that are also listed as ‘sufficient’ are not invoked. If it fails, the operation fails unless a module invoked after it succeeds. Important to note is that if a ‘required’ module fails before a ‘sufficient’ one succeeds, the operation will fail anyway, ignoring the status of and ‘sufficient’ modules
- include
- optional: mod is ignored unless all the other modules are in use. in they are not this one is invoke 