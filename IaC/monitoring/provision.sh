#!/usr/bin/env bash

# Update hosts file

echo "[TASK 1] Update /etc/hosts file"
cat >>/etc/hosts<<EOF
172.16.42.100 checkmk-srv
172.16.42.101 checkmk-client 
EOF

echo "[TASK 2] Installing requred software"
yum install -y wget openssl 

echo "[TASK 3] Downloading checkmk software"
wget https://download.checkmk.com/checkmk/2.0.0p9/check-mk-raw-2.0.0p9-el7-38.x86_64.rpm -P /home/vagrant/
