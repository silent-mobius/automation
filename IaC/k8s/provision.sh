#!/usr/bin/env bash 

modprobe br_netfilter
sudo sysctl -w net.bridge.bridge-nf-call-iptables=1
sudo sysctl -w net.ipv4.ip_forward=1

swapoff -a
sed -i 's/^\/swapfile*/#\/swapfile/' /etc/fstab

apt-get install docker.io -y
systemctl enable --now docker

curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
apt-get install -y apt-transport-https

echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" |tee -a /etc/apt/sources.list.d/kubernetes.list

apt-get update
apt-get install -y kubelet kubeadm kubectl

systemctl enable --now kubelet

systemctl restart kubelet

mkdir -p /var/lib/kubelet

echo 'KUBELET_KUBEADM_ARGS="--network-plugin=cni --pod-infra-container-image=k8s.gcr.io/pause:3.5 --cgroup-driver=cgroupfs"' >> /var/lib/kubelet/kubeadm-flags.env

kubeadm init --apiserver-advertise-address=10.0.0.99 --pod-network-cidr=10.244.0.0/16 

mkdir -p /var/lib/kubelet

echo 'KUBELET_KUBEADM_ARGS="--network-plugin=cni --pod-infra-container-image=k8s.gcr.io/pause:3.5 --cgroup-driver=cgroupfs"' >> /var/lib/kubelet/kubeadm-flags.env

mkdir -p /home/vagrant/.kube
sudo cp -i /etc/kubernetes/admin.conf /home/vagrant/.kube/config # needs to be substituted
sudo chown vagrant:vagrant /home/vagrant/.kube/config

kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml