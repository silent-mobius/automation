# Ldap Basics Lab

- ldap: Lightweight Directory Access Protocol (is it not an app) 
- open-ldap,ActiveDirectory and many others are implementation of ldap (these are the app)
- it is a protocol that access distributed directory survey. 
- it stores information regarding users, their authentication methods, and user specific information on the network, e.g home directory.
- ldap operates as directory tree (just like dns/bind)
- ldap features:
  - **Attribute**: one or more charactirics that make up an object
  - **Object**: a record for a single item in the directory
  - **Object Class**: collection of attributes that can be used to define object
  - **Object ID's**: dotted decimal heirarchy identifier
  - **CN** common name: name of the object
  - **DC** domain component: defines the components
  - **DN** distinguashed name: unique name that identifies the object in the LDAP tree
  - **LDIF** ldap data interchange format : a plain text representation of an ldap entry within an LDIF file
  - **Loglevel**: specifies the level at which events should be logged in the syslog
  
  - ldap tools and commands:
    - Slapd: the LDAP server daemon
    - Slapd-config: the configuration backend to slapd
    - Slapadd: add an entry to slapd database
    - Slapcat: the command to build an LDIF from the information in the slapd database
    - Slapindex: the command used to rebuild slapd indicies with the content of the slapd database
  
  - ldap files:
    - /etc/openldap/ldap.conf the configuration file for the file