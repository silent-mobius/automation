# Ansible Management Session

- Project designed to be taught in class or self paced
	- validate that you have dependencies for the project to work:
		- Linux OS (Debian/Rocky)
		- git
		- virtualbox/KVM/vmware-player
		- vagrant
	- clone the repo
	- move to FOLDER/TO/WHICH/CLONED/THE/REPO/automation/IaC/Ansible_Management
	- run command `vagrant up`
	- wait till it is done
