
$script= <<-SCRIPT
sed -i s/SELINUX=enforcing/SELINUX=permissive/ /etc/selinux/config

yum install -y epel-release

rpm --import https://pkg.jenkins.io/redhat/jenkins.io.key

yum install -y java-1.8.0-openjdk-devel curl git wget ansible openssh-server


sed -i s/#PubkeyAuthentication/PubkeyAuthentication/ /etc/ssh/sshd_config
sed -i s/#PasswordAuthentication/PasswordAuthentication/ /etc/ssh/sshd_config 

curl --silent --location http://pkg.jenkins-ci.org/redhat-stable/jenkins.repo | sudo tee /etc/yum.repos.d/jenkins.repo

yum install -y jenkins

systemctl enable jenkins
systemctl start jenkins

if [ -e /var/lib/jenkins/secrets/initialAdminPassword ];then
	cat /var/lib/jenkins/secrets/initialAdminPassword 
fi

 curl -L get.docker.com | bash

reboot
SCRIPT

$scr= <<-SCRIPT2

sed -i s/SELINUX=enforcing/SELINUX=permissive/ /etc/selinux/config

yum install -y epel-release

yum install -y java-1.8.0-openjdk-devel curl wget anisble git python3 python3-pip

useradd jenkins

sed -i s/#PubkeyAuthentication/PubkeyAuthentication/ /etc/ssh/sshd_config
sed -i s/#PasswordAuthentication/PasswordAuthentication/ /etc/ssh/sshd_config
sed -i s/PasswordAuthentication\ no/#PasswordAuthentication\ no/ /etc/ssh/sshd_config

systemctl restart sshd
 
wget -L get.docker.com | bash

SCRIPT2


Vagrant.configure("2") do |config|
  config.vm.define "jenkins" do |jenkins|
 	 jenkins.vm.box="almalinux/8"
	 jenkins.vm.hostname="jenkins-ci"
	 jenkins.vm.network "private_network", ip: "192.168.56.102"
  	 jenkins.vm.network "forwarded_port", guest: 8080, host: 8080
         jenkins.vm.provision "shell", inline: $script
	end
  config.vm.define "j" do|j|
 	 j.vm.box="almalinux/8"
	 j.vm.hostname="jenkins-worker"
	 j.vm.network "private_network", ip: "192.168.56.103"
         j.vm.provision "shell", inline: $scr
	end

	config.vm.define "test_subject" do |t|
	   t.vm.box="almalinux/8"
	   t.vm.hostname="jenkins-worker2"
	   t.vm.network "private_network", ip: "192.168.56.104"
	  end
end